## CSS blending mode
Blend mode -> multiple - screen
_css WebApp for different blending modes_

## Characters as images
```
background: url('data:image/svg+xml, \
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">\
  <text y="1em" font-size="90" transform="rotate(-21)">🦄</text>
</svg>');
background-size: 100px 100px;
```
## Fancy borders
```
<div class="🦄" contenteditable>look at my awesome border!</div>

.🦄 {
 url('data:image/svg+xml, \
<svg xmlns="http://www.w3.org/2000/svg">\
  <style>@keyframes ants {to {stroke-dashoffset: -15px}}</style>\
<rect width="100%" height="100%" style="stroke: black; stroke-width: 4px; fill: none;\
stroke-dasharray: 10px 5px; animation: ants .6s infinite linear"
</svg>');
}
```

## Descendant grid items
```
form label {
  display: contents
}
```
It will skip the item from the grid display

## Accessible CSS hover menus
```
nav li:focus-within ul {
  display: initial
}
```
## Variable fonts

## Responsive flex
```
article {
  display: flex;
  flex-wrap: wrap;
}

header {
  flex: 1;
}

.post {
  flex: 9999;
  min-width: 10em;
}
```
![IMG_20180626_112409.jpg](resources/608D8BE429B12DD4AE7D8E7745354B65.jpg)
## Grid autoflow
```
body {
  display: grid;
  --grid: repeat(4, 200px)
  grid-template: var(--grid) / var(--grid);
  grid-auto-flow: dense;
}

img {
  width: 100%;
  height: 100%;
  object-fit: cover;
}

img[src="=3"], img[src*="5"] {
  grid-column-end: span 2;
  grid-row-end: span 2;
}
```
![IMG_20180626_112751.jpg](resources/9B7AEF5C3CEC1479F1680B8B53C5DC19.jpg)
## Pie charts
```
.pie {
  padding: 20vmin;
  border-radius: 50%;
  --value: 30%;
  --value2: 70%;
  background: conic-gradient(red var(--value, 0%), yellow 0 var(--value2, 0%), yellowgreen 0);
}
```
![IMG_20180626_113144.jpg](resources/2F6A3C28D0D940F59A8832343CD23B36.jpg)