# Webpage test
- You can pass the attribute `?throtle_cpu=3.5` to webpage test to execute an stress test with different cpu on your results
- Number of test: try as high as possible to catch random anomalies tests might have
- Use Analytics to understand what matters to your tests
- In the filmstrip view use 0.1 sec interval to understand better the results 
- gzip enable for html
- Use a clean chrome profile (name it performance or testing. Tim and Harry have posts online about how to set this profile)

# Chrome DevTools
- Chrome start performance code usage
  - `css` unuse by double ckecking 
  - Criticial css inline / async stylesheet (practices)
- Javascript turn off chrome extension for quick chekc on loads
- `pre-connect` / `dns-prefetch`

_Note: check for new performance tips coming with HTTP2_