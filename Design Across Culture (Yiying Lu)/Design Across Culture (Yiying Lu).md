# Recommendations
- [The art of looking sideways](https://www.amazon.ca/Art-Looking-Sideways-Alan-Fletcher/dp/0714834491/)
- [Big Magic](https://www.amazon.ca/Big-Magic-Creative-Living-Beyond/dp/1594634718/)
- [The Elements of Typography Style](https://www.amazon.ca/Elements-Typographic-Style-Version-4-0/dp/0881792128)
- [The Brand Gap](https://www.amazon.ca/Brand-Gap-Revised-2nd/dp/0321348109)
- [It's Not How Good You Are, It's How Good You Want to Be: The world's best selling book](https://www.amazon.ca/Its-Not-How-Good-Want/dp/0714843377)
- [How to](https://www.amazon.ca/How-Michael-Bierut/dp/0062413902)