[Application github](https://github.com/marcysutton/empathy-driven-development)

## Tips 
- Main Header `role="banner"` for IE11
- `<main>` `role="main"` for IE11
- `<fieldset>` and use `<legend>` inside and wrapper your heading of that form to show what those fields are for
- Use color picker in the latest Chrome to get contrast ratio for a11y
- `role="dialog"` when you get something like a card modal but that doesn't stop the interactivity in the entire page. If that's the case use `role="modal-dialog"`

_Use what-input tool to change the styles based on the input you are using to interact with the UI. So, maybe outline doesn't show up on click but it does on key stroke_

- Use the new context API from React to handle animations On or Off in your app. 
- `@media screen and (prefers-reduced-motion) {}` to handle those preferences at the OS level. Currently macOS has the option not all of them. 