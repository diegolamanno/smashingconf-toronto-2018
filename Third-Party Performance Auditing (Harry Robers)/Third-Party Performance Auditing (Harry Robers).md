[Resources](https://gist.github.com/csswizardry/b8ca4f191f557fd958b975e7f6e44367)

# Chrome Tips
- Setup a chrome performance profile

# Webpage test tips 
- Always run the closest URL (avoid 301 redirects) when you are going to add it to webpagetest
- Webpage test force minimum duration in the _advaced_ tab if you are testing a SPA
- _Capture dev tools timeline_
- Yellow lines in the waterfall snapshot indicates redirect
- Vertical lines indicate website... more shallow line indicates starting to get third party stuff. Plus, all  the requrest come from with a ton of green/orange/purple prefixed which is the TCP connection
- Multiple for the same connection tends to be because it is running http1 and not http2
- In the file views. All the ones that are green background come before the rendering happens. Purples after render
- Run another run of webpagetest using the **block** tool and add all the third party domains you got from the mapping tool to test if they didnt' exist

# Devs tool tips
- Click double high row view 
- Values in size. Top value over the wire. Bottom disk (after uncrompessed)
- Time values. Top time to first bite. Bottom download time
- CMD shift P (panel) third party badges activate ON
- Group by frame toggle (to clear the view to see all files coming from sale third party)
- Hold shift and hover a file in the list. If it shows green in other file (those are files that requested the hovered one. If other ones show red color. Those are files downloaded becayse of the file hovered)
- Use the filter
 * prefix with `-` and you will get a list of preloaded filters
 * example: `-domain:*mec.ca` This will filter all files that aren't coming from `mec.ca`
- _Bottom-Up_ inside Performance tap after running a record you can group by domain to find third party elements or group by url to identify files
- Keep using the timeline. Moving will slowly show you what files are requested in order
- 
 

# Reuest Map Generator
- It piggy bags on webpagetest
- Size of the blob is the size of what's being downloaded
- Size of the line in the graffic to the main node is time to first bite
- Any sub branches. You can't do anything. Just focused on first nodes
- Donwload that from the page into a `.csv`

_awk_ tool regex sanitizer

_Note: you can fake a third party outdage by faking the host of one of your third parties so it doesn't load and show how your site will work without it. Probably not loading your page until chrome times out if you are not loading it async and it is blocking the rendering_